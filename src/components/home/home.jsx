import React, {useState} from "react";
import axios from "axios";
import "./home.css"
import {useNavigate} from 'react-router-dom';



const Home = () => {
    const urlRef = React.createRef();
    const navigate = useNavigate();
    const [uuid, setUuid]=useState()
    const handleSubmit = event => {
        event.preventDefault();
        navigate('/index', { state: {long_url:urlRef.current.value, uuid:uuid}, replace:false});
      };
    const shorten_url = async() => {
        const link = urlRef.current.value;
        const response = await axios.post("http://localhost:8000/shorten_url", {
            link
        })
    
        const short_url_uuid = response.data.uid
        setUuid(short_url_uuid)
        console.log("short_url_uuid: ", short_url_uuid)
    }
    
    return (
        <div className="container">
            <div className="header">
                <h1>Free Url Shortner</h1>
            </div>
            <div style={{paddingTop:'100px'}}>
                <form  className="form" onSubmit={handleSubmit}>
                    <label style={{padding:'20px', color:'#0c2431', textAlign:'center', fontWeight: "bold"}} htmlFor="Long Url">Shorten Your URL Here</label>
                    <input style={{height:'40px', width:'500px', textAlign:'center'}} type="Long Url" name="Long Url" id="Long Url" placeholder="Enter the long url" ref={urlRef}/>
                    <button onClick={shorten_url}>Submit</button>
                </form>
            </div>

            <div className="footer">
                <div className="copyright">
                    © 2022 Niharika
                </div>
            </div>
        </div>
    )
};

export default Home;
