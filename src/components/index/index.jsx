import React, { useEffect, useState } from "react";
import axios from "axios";
import "./index.css"
import { useNavigate, useLocation } from 'react-router-dom';



const Index = () => {
    const navigate = useNavigate()
    const { state } = useLocation()
    const [uuid, setUuid] = useState()
    const handleSubmit = event => {
        event.preventDefault();
        navigate('/home');
    };
    function myFunction() {
        /* Get the text field */
        var copyText = document.querySelector("#url");

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /* For mobile devices */

        /* Copy the text inside the text field */
        document.execCommand("copy");
    }

    function outFunc() {
        var tooltip = document.getElementById("myTooltip");
        tooltip.innerHTML = "Copy to clipboard";
    }
    useEffect(() => {
        get_shorten_url()
    }, [])

    const get_shorten_url = () => {
        const link = state.long_url;
        console.log("link: ", link)
        axios.post(`http://localhost:8000/get_shorten_url/?url=${link}`)
            // .then((res) => res.json())
            .then((result) => {
                setUuid(result.data.uid)
                return result.data.uid
            })
            .catch((err) => console.log('error: ', err))
    }
    if (!uuid) return null

    return (
        <>
            <meta charSet="UTF-8" />
            <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <title>Url Shortner</title>
            <link
                rel="stylesheet"
                href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
            />
            <div className="containers">
                <div className="header">
                    <h1>Free Url Shortner</h1>
                </div>
                <div style={{ paddingLeft: '50px', paddingTop: '100px' }}>
                    <form method="POST">
                        <h2 style={{ paddingRight: '100px', color: '#0c2431' }}>Collect your shortened url</h2>
                        <div className="op-link">
                            <input style={{ height: '40px', width: '250px', textAlign: 'center' }}
                                id="url"
                                defaultValue={`http://127.0.0.1:8000/short/${uuid}`}
                                className="form-control"
                                readOnly=""
                            />
                            <div className="tooltip">
                                <div onClick={myFunction} onMouseOut={outFunc}>
                                    <span className="tooltiptext" id="myTooltip">
                                        Copy to clipboard
                                    </span>
                                    <i className="fa fa-clipboard" />
                                </div>
                            </div>
                        </div>
                        <button onClick={handleSubmit}>Go Back</button>
                    </form>
                </div>
                <div className="footer">
                    <div className="copyright">© 2022 Niharika</div>
                </div>
            </div>
        </>
    )
};

export default Index;
