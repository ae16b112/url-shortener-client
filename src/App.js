import './App.css';
import Home from './components/home/home';
import {Routes, Route} from 'react-router-dom';
import Index from './components/index/index';

function App() {
  return (
    <div>
      <div>
        <Routes>
          <Route path="/index" element={<Index />} />
          <Route path="/home" element={<Home />} />
        </Routes>
      </div>
    </div>
  );
}

export default App;
